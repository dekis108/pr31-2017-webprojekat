﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class EventPlace
    {
        public string Street { get; set; }
        public string Town { get; set; }
        public string ZipCode { get; set; }

        public override bool Equals(object obj)
        {
            EventPlace place = obj as EventPlace;
            if (place is null)
            {
                return false;
            }

            return place.Street == Street && place.Town == Town && place.ZipCode == ZipCode;
        }
    }
}