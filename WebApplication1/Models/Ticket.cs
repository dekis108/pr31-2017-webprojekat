﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public enum TicketType { REGULAR = 1, FANPIT = 2, VIP = 4};
    
    public enum TicketStatus { Reserved , Forfeit }

    public class Ticket
    {
        public string Id { get; set; } //10 long

        public Manifestation Manifestation { get; set; }

        public DateTime EventTime {
            get
            {
                return Manifestation.EventTime;
            }
        }

        public double Cost { get; set; }

        public TicketType Type { get; set; }

        public string CustomerUsername { get; set; }

        public string CustomerName { get; set; }

        public string CustomerLastname { get; set; }

        public TicketStatus Status { get; set; }

    }
}