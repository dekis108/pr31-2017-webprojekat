﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public sealed class DefinedUserTypes
    {


        private static DefinedUserTypes _instance;
        private static Object lockObject = new Object();
        public List<UserType> UserTypes { get; }

        private DefinedUserTypes()
        {
            UserTypes = new List<UserType>()
            {
                new UserType()
                {
                    Name = "Basic",
                    Discount = 1,
                    TargetPoints = 0
                },
                new UserType()
                {
                    Name = "Party Goer",
                    Discount = 0.95,
                    TargetPoints = 1
                },
                new UserType()
                {
                    Name = "Hardcore",
                    Discount = 0.70,
                    TargetPoints = 500
                }
            };

        }




        public static DefinedUserTypes Instance()
        {
            if (_instance == null)
            {
                lock (lockObject)
                    {
                        if (_instance == null)
                        {
                            _instance = new DefinedUserTypes();
                        }
                    }
            }
            return _instance;
        }


        
    }
}