﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class UserType
    {
        public string Name { get; set; }

        public double Discount { get; set; }

        public int TargetPoints { get; set; }
    }
}