﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public enum Gender { Male = 0, Female };
    public enum Role { Customer = 0, Organiser, Admin }

    public class User
    {
        public bool Active { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 3)]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 8)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 1)]
        public string Name { get; set; }


        [Required]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 1)]

        public string Lastname { get; set; }

        [Required]
        public Gender Gender { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        public Role Role { get; set; }

        public List<Ticket> Tickets { get; set; }

        public List<Manifestation> Manifestations { get; set; }

        public int Points { get; set; } 

        public UserType Type { get; set; }

        public User()
        {
            Tickets = new List<Ticket>();
            Manifestations = new List<Manifestation>();
        }


        public bool CheckPromotion()
        {
            UserType newType = DefinedUserTypes.Instance().UserTypes.FindLast(x => x.TargetPoints <= Points);

            if (newType != null && Type != newType && newType.Discount < Type.Discount) //promotion
            {
                Type = newType;
                return true;
            }
            return false;
        }
    }
}