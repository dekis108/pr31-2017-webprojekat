﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Models
{
    public static class Storage
    {
        public static string UserPath = "~/App_Data/Users.txt";
        public static string ManifPath = "~/App_Data/Manifestations.txt";
        public static string TicketsPath = "~/App_Data/Tickets.txt";


        public static List<User> LoadUsers()
        {
            List<User> users = new List<User>();
            string line = "";
            using (StreamReader sr = new StreamReader(HostingEnvironment.MapPath(UserPath)))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    string[] tokens = line.Split(';');
                    Gender g;
                    DateTime dob;
                    Role role;
                    bool active;
                    UserType type;
                    double discount;
                    int points, target;
                    try
                    {
                        switch (tokens[0])
                        {
                            case "True":
                                active = true;
                                break;
                            case "False":
                                active = false;
                                break;
                            default:
                                throw new ArgumentException("Activity string is incorrect.");
                        }
                        g = (Gender)Enum.Parse(typeof(Gender), tokens[5]);
                        dob = DateTime.Parse(DateTime.Parse(tokens[6]).ToString("dd/MM/yyyy"));
                        role = (Role)Enum.Parse(typeof(Role), tokens[7]);
                        points = int.Parse(tokens[8]);
                        discount = double.Parse(tokens[10]);
                        target = int.Parse(tokens[11]);
                        type = new UserType()
                        {
                            Name = tokens[9],
                            Discount = discount,
                            TargetPoints = target
                        };
                    }
                    catch(Exception e)
                    {
                        Debug.WriteLine(e);
                        continue;
                    }
                    User user = new User()
                    {
                        Active = active,
                        Username = tokens[1],
                        Password = tokens[2],
                        Name = tokens[3],
                        Lastname = tokens[4],
                        Gender = g,
                        DateOfBirth = dob,
                        Role = role,
                        Points = points,
                        Type = type
                    };
                    users.Add(user);

                    Debug.WriteLine("[DEBUG][{0}] Loaded user: {1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}",
                            DateTime.Now, user.Active.ToString(), user.Username, user.Password, user.Name, user.Lastname
                        , user.Gender.ToString(), user.DateOfBirth, user.Role.ToString(), user.Points, user.Type.Name
                        , user.Type.Discount, user.Type.TargetPoints);

                }
            }
            return users;
        }

        public static void SaveUsers(List<User> users)
        {
            using (StreamWriter sw = new StreamWriter(HostingEnvironment.MapPath(UserPath), false))
            {
                foreach (User user in users)
                {
                    sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}"
                        , user.Active.ToString(), user.Username, user.Password, user.Name, user.Lastname
                        , user.Gender.ToString(), user.DateOfBirth, user.Role.ToString(), user.Points, user.Type.Name
                        , user.Type.Discount, user.Type.TargetPoints);

                    Debug.WriteLine("[DEBUG][{0}] Saved user: {1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}" +
                        "   to memory", DateTime.Now, user.Active.ToString(), user.Username, user.Password, user.Name, user.Lastname
                        , user.Gender.ToString(), user.DateOfBirth, user.Role.ToString(), user.Points, user.Type.Name
                        , user.Type.Discount, user.Type.TargetPoints);
                }
            }
        }

        public static void SaveUser(User user)
        {
            using (StreamWriter sw = new StreamWriter(HostingEnvironment.MapPath(UserPath), true))
            {
                sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}"
                    , user.Active.ToString(), user.Username, user.Password, user.Name, user.Lastname
                    , user.Gender.ToString(), user.DateOfBirth, user.Role.ToString(), user.Points, user.Type.Name
                    , user.Type.Discount, user.Type.TargetPoints);

                Debug.WriteLine("[DEBUG][{0}] Saved user: {1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}" +
                    "   to memory", DateTime.Now, user.Active.ToString(), user.Username, user.Password, user.Name, user.Lastname
                    , user.Gender.ToString(), user.DateOfBirth, user.Role.ToString(), user.Points, user.Type.Name
                    , user.Type.Discount, user.Type.TargetPoints);
            }
        }

        public static List<Manifestation> LoadManifestations()
        {
            List<User> users = (List<User>)HttpContext.Current.Application["users"];
            List<Manifestation> manifestations = new List<Manifestation>();
            using (StreamReader sr = new StreamReader(HostingEnvironment.MapPath(ManifPath)))
            {
                string data = sr.ReadToEnd();
                List<string> lines = data.Split( new[] { Environment.NewLine }, StringSplitOptions.None).ToList();

                for (int i = 0; i < lines.Count; ++i)
                {
                    string[] tokens = lines[i].Split(';');
                    if (tokens[0] == "\tCOMMENT")
                    {
                        continue;
                    }

                    bool activity;
                    ManifestationType type;
                    int vipCount, regularCount, fanPitCount, cost;
                    DateTime dateTime;
                    Location location;
                    List<Comment> comments = new List<Comment>();

                    try
                    {
                        switch (tokens[1])
                        {
                            case "True":
                                activity = true;
                                break;
                            case "False":
                                activity = false;
                                break;
                            default:
                                throw new ArgumentException("Activity string is incorrect.");
                        }
                        type = (ManifestationType)Enum.Parse(typeof(ManifestationType), tokens[2]);
                        //seatCount = int.Parse(tokens[3]);
                        regularCount = int.Parse(tokens[3]);
                        fanPitCount = int.Parse(tokens[4]);
                        vipCount = int.Parse(tokens[5]);
                        dateTime = DateTime.Parse(DateTime.Parse(tokens[6]).ToString());
                        cost = int.Parse(tokens[7]);
                        EventPlace eventPlace = new EventPlace()
                        {
                            Street = tokens[8],
                            Town = tokens[9],
                            ZipCode = tokens[10]
                        };
                        location = new Location()
                        {
                            GeoHeight = double.Parse(tokens[11]),
                            GeoWidht = double.Parse(tokens[12]),
                            Place = eventPlace
                        };
                
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e);
                        continue;
                    }
                    Manifestation manif = new Manifestation()
                    {
                        Name = tokens[0],
                        Active = activity,
                        Type = type,
                        //SeatCount = seatCount,
                        FanPitSeats = fanPitCount,
                        RegularSeats = regularCount,
                        VipSeats = vipCount,
                        EventTime = dateTime,
                        RegularCost = cost,
                        Location = location,
                        OrganiserUsername = tokens[13],
                        ImagePath = tokens[14],
                        Comments = comments
                    };
                    //e sad idemo da se rukujemo s djavolom
                    for (int k = i + 1; k < lines.Count; ++k)
                    {
                        string[] tokensInner = lines[k].Split(';');
                        if (tokensInner[0] != "\tCOMMENT")
                        {
                            break;
                        }

                        //sad valjda imamo komentar u rukama
                        User user;
                        bool cActive;
                        int score;
                        try
                        {
                            switch (tokensInner[1])
                            {
                                case "True":
                                    cActive = true;
                                    break;
                                case "False":
                                    cActive = false;
                                    break;
                                default:
                                    throw new ArgumentException("Activity string is incorrect.");
                            }
                            user = users.Find(x => x.Username == tokensInner[2]);
                            if (manif.Name != tokensInner[3])
                            {
                                throw new ArgumentException("Someone elses comment?");
                            }
                            score = int.Parse(tokensInner[5]);
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine("Exception in comment hadnling: " + e);
                            continue;
                        }

                        Comment comment = new Comment()
                        {
                            Active = cActive,
                            User = user,
                            Manifestation = manif,
                            Text = tokensInner[4],
                            Score = score
                        };
                        manif.Comments.Add(comment);
                    }
                    manifestations.Add(manif);
                }
            }
            return manifestations.OrderBy(x => x.EventTime).ToList(); //mozda treba obrnuto sort
        }

        public static void SaveManifestation(Manifestation manif)
        {
            using (StreamWriter sw = new StreamWriter(HostingEnvironment.MapPath(ManifPath), true))
            {
                sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14}",
                    manif.Name, manif.Active.ToString(), manif.Type.ToString(), manif.RegularSeats,
                    manif.FanPitSeats, manif.VipSeats , 
                    manif.EventTime.ToString(), manif.RegularCost, manif.Location.Place.Street,
                    manif.Location.Place.Town, manif.Location.Place.ZipCode, manif.Location.GeoHeight, 
                    manif.Location.GeoWidht, manif.OrganiserUsername, manif.ImagePath);

                //save comments
                foreach(Comment comment in manif.Comments)
                {
                    sw.WriteLine("\tCOMMENT;{0};{1};{2};{3};{4}",
                        comment.Active.ToString(), comment.User.Username, comment.Manifestation.Name,
                        comment.Text, comment.Score);
                }


            }
        }

        public static void SaveManifestations(List<Manifestation> manifestations)
        {
            using (StreamWriter sw = new StreamWriter(HostingEnvironment.MapPath(ManifPath), false))
            {
                foreach (Manifestation manif in manifestations)
                {
                    sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14}",
                        manif.Name, manif.Active.ToString(), manif.Type.ToString(), manif.RegularSeats,
                        manif.FanPitSeats, manif.VipSeats,
                        manif.EventTime.ToString(), manif.RegularCost, manif.Location.Place.Street,
                        manif.Location.Place.Town, manif.Location.Place.ZipCode, manif.Location.GeoHeight,
                        manif.Location.GeoWidht, manif.OrganiserUsername, manif.ImagePath);

                    //save comments
                    foreach (Comment comment in manif.Comments)
                    {
                        sw.WriteLine("\tCOMMENT;{0};{1};{2};{3};{4}",
                            comment.Active.ToString(), comment.User.Username, comment.Manifestation.Name,
                            comment.Text, comment.Score);
                    }

                }
            }
        }

        public static void SaveTickets(List<Ticket> tickets)
        {
            using (StreamWriter sw = new StreamWriter(HostingEnvironment.MapPath(TicketsPath), false))
            {
                foreach (Ticket ticket in tickets)
                {
                    sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7}",
                            ticket.Id, ticket.CustomerUsername, ticket.Cost, ticket.Manifestation.Name, ticket.Type.ToString(), ticket.CustomerName,
                            ticket.CustomerLastname, ticket.Status.ToString());
                }
            }
        }

        public static void SaveTicket(Ticket ticket)
        {
            using (StreamWriter sw = new StreamWriter(HostingEnvironment.MapPath(TicketsPath), true))
            {
                sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7}",
                        ticket.Id,ticket.CustomerUsername ,ticket.Cost ,ticket.Manifestation.Name, ticket.Type.ToString(), ticket.CustomerName,
                        ticket.CustomerLastname, ticket.Status.ToString());

            }
        }

        public static List<Ticket> LoadTickets()
        {
            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Current.Application["manifestations"];
            List<Ticket> tickets = new List<Ticket>();
            using (StreamReader sr = new StreamReader(HostingEnvironment.MapPath(TicketsPath)))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] token = line.Split(';');
                    Manifestation manif;
                    TicketType type;
                    TicketStatus status;
                    double cost;
                    try
                    {
                        cost = double.Parse(token[2]);
                        manif = manifestations.Find(x => x.Name == token[3]);
                        if (manif is null)
                        {
                            throw new ArgumentNullException("Manifestation non-existing.");
                        }
                        type = (TicketType)Enum.Parse(typeof(TicketType), token[4]);

                        status = (TicketStatus)Enum.Parse(typeof(TicketStatus), token[7]);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e);
                        continue;
                    }
                    Ticket ticket = new Ticket()
                    {
                        Id = token[0],
                        CustomerUsername = token[1],
                        Cost = cost,
                        Manifestation = manif,
                        Type = type,
                        CustomerName = token[5],
                        CustomerLastname = token[6],
                        Status = status
                    };
                    tickets.Add(ticket);
                }
            }
            return tickets;
        }
    }
}