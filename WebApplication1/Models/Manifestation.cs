﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public enum ManifestationType { Concert = 0, Festival, Cinema}

    public class Manifestation
    {
        [Required]
        [StringLength(30, MinimumLength = 3)]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        public bool Active { get; set; }

        public ManifestationType Type { get; set; }

        public int RegularSeats { get; set; }

        public int VipSeats { get; set; }

        public int FanPitSeats { get; set; }


        public int SeatCount
        {
            get
            {
                return VipSeats + FanPitSeats + RegularSeats;
            }
        }

        [Required]
        public DateTime EventTime { get; set; }


        [Required]
        public double RegularCost { get; set; }

        public double VipCost
        {
            get
            {
                return RegularCost * 4;
            }
        }

        public double PitCost
        {
            get
            {
                return RegularCost * 2;
            }
        }

        public Location Location { get; set; }

        public string ImagePath { get; set; }

        public string OrganiserUsername { get; set; }


        public List<Comment> Comments { get; set;  }


        public Manifestation()
        {
            Comments = new List<Comment>();
        }

      

        public double? AverageGrade()
        {
            if (EventTime > DateTime.Now)
            {
                return null;
            }
            double grade = 0;
            foreach(var comment in Comments)
            {
                grade += comment.Score;
            }
            return grade / Comments.Count;
        }

        public int RewardPoints(int Count, TicketType Type)
        {
            return (int)(Count * RegularCost * (int)Type/1000 * 133);
        }

        public double TotalCost(int Count, TicketType Type, UserType userType)
        {
            return Count * RegularCost * (int)Type * userType.Discount;
        }
        
    }
}