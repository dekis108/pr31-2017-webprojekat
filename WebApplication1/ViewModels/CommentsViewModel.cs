﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
    public class CommentsViewModel
    {
        public User User { get; set; }
        public Manifestation Manifestation { get; set; }
    }
}