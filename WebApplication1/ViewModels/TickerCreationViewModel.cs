﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
    public class TickerCreationViewModel
    {
        public User User { get; set; }
        public Manifestation Manifestation { get; set; }
        public int Count { get; set; }
        public TicketType TicketType { get; set; }
        public double TotalCost { get; set; }
    }
}