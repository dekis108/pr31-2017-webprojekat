﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApplication1.Models;

namespace WebApplication1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            HttpContext.Current.Application["SortUsername"] = false;
            HttpContext.Current.Application["SortName"] = false;
            HttpContext.Current.Application["SortLastname"] = false;
            HttpContext.Current.Application["SortPoints"] = false;
            HttpContext.Current.Application["SortNameManifestation"] = false;
            HttpContext.Current.Application["SortLocationManifestation"] = false;
            HttpContext.Current.Application["SortCostManifestation"] = false;
            HttpContext.Current.Application["SortTimeManifestation"] = false;


            HttpContext.Current.Application["FilteredUsers"] = false;
            HttpContext.Current.Application["FilteredManifestations"] = false;

            List<User> users = Storage.LoadUsers();
            HttpContext.Current.Application["users"] = users;

            List<Manifestation> manifestations = Storage.LoadManifestations();
            HttpContext.Current.Application["manifestations"] = manifestations;

            List<Ticket> tickets = Storage.LoadTickets();
            HttpContext.Current.Application["tickets"] = tickets;



            //assigning tickets and manifestations to users
            for (int i = 0; i < users.Count; ++i)
            {
                List<Ticket> userTickets = tickets.Where(x => x.CustomerUsername == users[i].Username).ToList();
                users[i].Tickets = userTickets;

                List<Manifestation> userManifs = manifestations.Where(x => x.OrganiserUsername == users[i].Username).ToList();
                users[i].Manifestations = userManifs;
            }



        }
    }
}
