﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Admin)
                return RedirectToAction("Index", "Authentication");

            List<User> users = (List<User>)HttpContext.Application["users"];

            return View(users);
        }

        [HttpPost]
        public ActionResult DeleteOrRestoreUser(string username)
        {

            Debug.WriteLine("[DEBUG][{0}] DeleteOrRestoreUser called for username: {1}",
                DateTime.Now, username);

            List<User> users = (List<User>)HttpContext.Application["users"];

            User user = users.Find(x => x.Username == username);
            if (!(user is null))
            {
                user.Active = !user.Active;
            }
            //users.Where(x => x.Username == username) = user;
            users[users.FindIndex(x => x.Username == user.Username)] = user;
            HttpContext.Application["users"] = users;
            Storage.SaveUsers(users);
            return RedirectToAction("Index");
        }

        public ActionResult CreateOrganiser()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Admin)
                return RedirectToAction("Index", "Authentication");

            return View();

        }

        [HttpPost]
        public ActionResult RegisterOrganiser(User organiser)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Admin)
                return RedirectToAction("Index", "Authentication");

            organiser.Role = Role.Organiser;
            organiser.Manifestations = new List<Manifestation>();
            organiser.Active = true;

            UserType type = new UserType()
            {
                Name = "Basic",
                Discount = 1,
                TargetPoints = 0
            };
            organiser.Type = type;

            List<User> users = (List<User>)HttpContext.Application["users"];
            users.Add(organiser);
            HttpContext.Application["users"] = users;

            Storage.SaveUser(organiser);

            return RedirectToAction("Index");
        }


        public ActionResult AdminManifestation()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Role != Role.Admin)
            {
                return RedirectToAction("Index", "Manifestations"); 
            }

            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            return View(manifestations);
        }

        [HttpPost]
        public ActionResult ChangeManifestation(string Name)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Role != Role.Admin)
            {
                return RedirectToAction("Index", "Manifestations");
            }

            Debug.WriteLine("[DEBUG][{0}] ChangeManifestation called for name: {1}",
                DateTime.Now, Name);

            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];

            Manifestation manif = manifestations.Find(x => x.Name == Name);
            if (!(manif is null))
            {
                manif.Active = !manif.Active;
            }

            manifestations[manifestations.FindIndex(x => x.Name == manif.Name)] = manif;
            HttpContext.Application["manifestations"] = manifestations;
            Storage.SaveManifestations(manifestations);
            return RedirectToAction("AdminManifestation");
        }


        [HttpPost]
        public ActionResult FilterUsers(string Username, string Name, string Lastname, string UserRole, string Type)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Role != Role.Admin)
            {
                return RedirectToAction("Index", "Manifestations");
            }



            List<User> users = (List<User>)HttpContext.Application["users"];
            List<User> filteredUsers = users.Where(x => x.Role != Role.Admin).ToList();

            if (UserRole != null && UserRole != "")
            {
                Role role = (Role)Enum.Parse(typeof(Role), UserRole);
                filteredUsers = filteredUsers.Where(x => x.Role == role).ToList();
            }

            if (Type != null && Type != "")
            {
                filteredUsers = filteredUsers.Where(x => x.Type.Name == Type).ToList();
            }


            if (Username != null && Username != "")
            {
                filteredUsers = filteredUsers.Where(x =>
                    x.Username.Contains(Username)).ToList();
            }

            if (Name != null && Name != "")
            {
                filteredUsers = filteredUsers.Where(x =>
                    x.Name.Contains(Name)).ToList();
            }

            if (Lastname != null && Lastname != "")
            {
                filteredUsers = filteredUsers.Where(x =>
                    x.Lastname.Contains(Lastname)).ToList();
            }

            if ((Username == null || Username == "") && (Name == null || Name == "")
                 && (Lastname == null || Lastname == ""))
            {
                HttpContext.Application["FilteredUsers"] = false;
            }
            else
            {
                HttpContext.Application["FilteredUsers"] = true;
                HttpContext.Application["FilteredUsersList"] = filteredUsers;
            }


            return View("Index", filteredUsers);
        }

        public ActionResult SortUsername()
        {

            List<User> users = (List<User>)HttpContext.Application["users"];

            bool filtered = (bool)HttpContext.Application["FilteredUsers"];
            if (filtered)
            {
                users = (List<User>)HttpContext.Application["FilteredUsersList"];
            }


            bool sorted = (bool)HttpContext.Application["SortUsername"];
            
            if (sorted)
            {
                users = (List<User>)HttpContext.Application["usersSortedUsername"];
                users.Reverse(); 
                HttpContext.Application["SortUsername"] = false;
            }
            else
            {
                users = users.OrderBy(x => x.Username).ToList();
                HttpContext.Application["usersSortedUsername"] = users;
                HttpContext.Application["SortUsername"] = true;
            }
            HttpContext.Application["SortName"] = false;
            HttpContext.Application["SortLastname"] = false;
            HttpContext.Application["SortPoints"] = false;

            return View("Index", users);
        }

        public ActionResult SortName()
        {

            List<User> users = (List<User>)HttpContext.Application["users"];

            bool filtered = (bool)HttpContext.Application["FilteredUsers"];
            if (filtered)
            {
                users = (List<User>)HttpContext.Application["FilteredUsersList"];
            }


            bool sorted = (bool)HttpContext.Application["SortName"];

            if (sorted)
            {
                users = (List<User>)HttpContext.Application["usersSortedName"];
                users.Reverse();
                HttpContext.Application["SortName"] = false;
            }
            else
            {
                users = users.OrderBy(x => x.Username).ToList();
                HttpContext.Application["usersSortedName"] = users;
                HttpContext.Application["SortName"] = true;
            }
            HttpContext.Application["SortLastname"] = false;
            HttpContext.Application["SortPoints"] = false;
            HttpContext.Application["SortUsername"] = false;

            return View("Index", users);
        }

        public ActionResult SortLastname()
        {

            List<User> users = (List<User>)HttpContext.Application["users"];

            bool filtered = (bool)HttpContext.Application["FilteredUsers"];
            if (filtered)
            {
                users = (List<User>)HttpContext.Application["FilteredUsersList"];
            }


            bool sorted = (bool)HttpContext.Application["SortLastname"];

            if (sorted)
            {
                users = (List<User>)HttpContext.Application["usersSortedLastname"];
                users.Reverse();
                HttpContext.Application["SortLastname"] = false;
            }
            else
            {
                users = users.OrderBy(x => x.Username).ToList();
                HttpContext.Application["usersSortedLastname"] = users;
                HttpContext.Application["SortLastname"] = true;
            }
            HttpContext.Application["SortName"] = false;
            HttpContext.Application["SortPoints"] = false;
            HttpContext.Application["SortUsername"] = false;

            return View("Index", users);
        }

        public ActionResult SortPoints()
        {
            List<User> users = (List<User>)HttpContext.Application["users"];

            bool filtered = (bool)HttpContext.Application["FilteredUsers"];
            if (filtered)
            {
                users = (List<User>)HttpContext.Application["FilteredUsersList"];
            }


            bool sorted = (bool)HttpContext.Application["SortPoints"];

            if (sorted)
            {
                users = (List<User>)HttpContext.Application["usersSortedPoints"];
                users.Reverse();
                HttpContext.Application["SortPoints"] = false;
            }
            else
            {
                users = users.OrderBy(x => x.Points).ToList();
                HttpContext.Application["usersSortedPoints"] = users;
                HttpContext.Application["SortPoints"] = true;
            }
            HttpContext.Application["SortName"] = false;
            HttpContext.Application["SortLastname"] = false;
            HttpContext.Application["SortUsername"] = false;

            return View("Index", users);
        }
    }
}