﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class ManifestationsController : Controller
    {
        // GET: Manifestations
        public ActionResult Index()
        {
            User user = (User)Session["UserData"];
            if (user != null && user.Role == Role.Admin)
            { 
                return RedirectToAction("AdminManifestation", "Admin"); //admin view
            }

            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            return View(manifestations);
        }

        public ActionResult CreateManifestation()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Organiser)
                return RedirectToAction("Index", "Authentication");



            return View();
        }

        [HttpPost]
        public ActionResult SubmitManifestation(Manifestation manif, string ZipCode, string Town, string Street,
            string GeoWidht, string GeoHeight, string Time, string imgPath)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Organiser)
                return RedirectToAction("Index", "Authentication");

            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            if (manifestations.Exists(x => x.Name == manif.Name)) 
            {
                List<string> msgs = new List<string>
                {
                    "Name taken."
                };
                return View("ManifestationsError", msgs);
            }

          

            if (!ModelState.IsValid)
            {
                Debug.WriteLine("[DEBUG][{0}] Invalid manifestation registration", DateTime.Now);
                var errors = ModelState.Select(x => x.Value.Errors)
                       .Where(y => y.Count > 0).ToList();
                List<string> errorMsg = new List<string>();
                foreach (var error in errors)
                {
                    foreach (var e in error)
                    {
                        Debug.WriteLine(e.ErrorMessage);
                        errorMsg.Add(e.ErrorMessage);
                    }

                }
                return View("ManifestationsError", errorMsg);

            }



            EventPlace place = new EventPlace()
            {
                ZipCode = ZipCode,
                Town = Town,
                Street = Street
            };
            Location location = new Location()
            {
                Place = place,
                GeoWidht = double.Parse(GeoWidht), 
                GeoHeight = double.Parse(GeoHeight)
            };
            manif.Location = location;

            if (manifestations.Exists(x => x.Location.Place.Equals(manif.Location.Place) 
                && x.EventTime.ToShortDateString() == manif.EventTime.ToShortDateString()))
            {
                List<string> msgs = new List<string>
                {
                    "That location is arleady taken for that date."
                };
                return View("ManifestationsError", msgs);
            }

            manif.EventTime = DateTime.Parse(manif.EventTime.ToShortDateString() + " " + Time);
            manif.Comments = new List<Comment>();
            manif.ImagePath = imgPath;
            manif.OrganiserUsername = user.Username;

            manifestations.Add(manif);
            HttpContext.Application["manifestations"] = manifestations;
            Storage.SaveManifestation(manif);

            user.Manifestations.Add(manif);

            return RedirectToAction("Index");
        }


        [HttpPost]
        public ActionResult Details(string Name)
        {
            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            var manif = manifestations.Find(x => x.Name == Name);
            if (manif is null)
            {
                return RedirectToAction("Index");
            }

            return View(manif);
        }

        [HttpPost]
        public ActionResult ReserveTicket(string Name)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Customer)
                return RedirectToAction("Index", "Authentication");

            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];

            Manifestation manif = manifestations.Find(x => x.Name == Name);

            if (manif is null)
            {
                return RedirectToAction("Index");
            }

            if (manif.SeatCount < 1)
            {
                //TODO: napravi mu error msg (mozda jos bolje zasivi mu Reserve dugme)
            }

            TickerCreationViewModel vm = new TickerCreationViewModel()
            {
                User = user,
                Manifestation = manif
            };

            return View("ReserveTicket", vm);
        }


        [HttpPost]
        public ActionResult Checkout(string Type, int Count, string ManifestationName, string UserName)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Customer)
                return RedirectToAction("Index", "Authentication");


            TicketType ticketType;
            try
            {
                ticketType = (TicketType)Enum.Parse(typeof(TicketType), Type);
            }
            catch
            {
                List<string> errorMsg = new List<string>()
                {
                    "Invalid ticket type selection."
                };
                return View("ManifestationsError", errorMsg);
            }
            

            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            Manifestation manif = manifestations.Find(x => x.Name == ManifestationName);

            if (manif.EventTime < DateTime.Now)
            {
                List<string> errorMsg = new List<string>()
                {
                    "Manifestation is over."
                };
                return View("ManifestationsError", errorMsg);
            }

            bool invalid = false;
            switch(ticketType)
            {
                case TicketType.FANPIT:
                    if (manif.FanPitSeats < Count)
                    {
                        invalid = true;
                    }
                    break;
                case TicketType.REGULAR:
                    if (manif.RegularSeats < Count)
                    {
                        invalid = true;
                    }
                    break;
                case TicketType.VIP:
                    if (manif.VipSeats < Count)
                    {
                        invalid = true;
                    }
                    break;
            }
            if (invalid)
            {
                List<string> errorMsg = new List<string>()
                {
                    "Not enought free seats"
                };
                return View("ManifestationsError", errorMsg);
            }




            if (manif is null)
            {
                return RedirectToAction("Index"); //TODO: error msg
            }

            TickerCreationViewModel Model = new TickerCreationViewModel()
            {
                User = user,
                Manifestation = manif,
                Count = Count,
                TicketType = ticketType,
                TotalCost = manif.TotalCost(Count, ticketType, user.Type) 
            };
            return View(Model);
        }

        [HttpPost]
        public ActionResult ConfirmPurchase(string ManifestationName, string TotalCost, string Count,
            string Type)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Customer)
                return RedirectToAction("Index", "Authentication");

            //not validating here because it must have been validated at the previous step
            TicketType ticketType = (TicketType)Enum.Parse(typeof(TicketType), Type);

            List<User> users = (List<User>)HttpContext.Application["users"];
            List<Ticket> tickets = (List<Ticket>)HttpContext.Application["tickets"];
            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            Manifestation manif = manifestations.Find(x => x.Name == ManifestationName);

            int count = int.Parse(Count);
            List<Ticket> ticketsToAdd = new List<Ticket>();
            for(int i = 0; i < count; ++i)
            {
                Ticket ticket = new Ticket()
                {
                    //Id = user.Username + manif.Name + (i + user.Tickets.Count),
                    Id = Guid.NewGuid().ToString(),
                    CustomerName = user.Name,
                    CustomerUsername = user.Username,
                    CustomerLastname = user.Lastname,
                    Type = ticketType,
                    Manifestation = manif,
                    Cost = manif.TotalCost(1, ticketType, user.Type),
                    Status = TicketStatus.Reserved
                };

                user.Tickets.Add(ticket);
                ticketsToAdd.Add(ticket);

            }
            tickets.AddRange(ticketsToAdd);
            Storage.SaveTickets(tickets);

            switch(ticketType)
            {
                case TicketType.VIP:
                    manif.VipSeats--;
                    break;
                case TicketType.REGULAR:
                    manif.RegularSeats--;
                    break;
                case TicketType.FANPIT:
                    manif.FanPitSeats--;
                    break;
            }


            Storage.SaveManifestations(manifestations);

            user.Points += manif.RewardPoints(count, ticketType);
            if (user.CheckPromotion())  //save the change to the user promotion if it happened
            {
                User toUpdate = users.Find(x => x.Username == user.Username);
                toUpdate.Points = user.Points;
                toUpdate.Type = user.Type;
                toUpdate.Tickets = user.Tickets;
                Storage.SaveUsers(users);
            }
            
            


            return RedirectToAction("Index");
        }

        public ActionResult DisplayManifestations()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Organiser)
                return RedirectToAction("Index", "Authentication");

            return View(user.Manifestations);
        }

        public ActionResult TicketsForManifestation(string Name)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Organiser)
                return RedirectToAction("Index", "Authentication");

            List<Ticket> tickets = (List<Ticket>)HttpContext.Application["tickets"];
            List<Ticket> manifTickets = tickets.Where(x => x.Manifestation.Name == Name).ToList();

            return View(manifTickets);
        }

        [HttpPost]
        public ActionResult ChangeManifestation(string Name)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Organiser)
                return RedirectToAction("Index", "Authentication");

            Manifestation manif =  user.Manifestations.Find(x => x.Name == Name);
            if (manif is null)
            {
                return RedirectToAction("DisplayManifestations");
            }

            return View(manif);
        }

        [HttpPost]
        public ActionResult EditManifestation(Manifestation manif, string imgPath, string Street, 
            string Town, string ZipCode, string GeoHeight, string GeoWidht, string Name, string Time, string OldPath)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Organiser)
                return RedirectToAction("Index", "Authentication");
            
            if (imgPath is null || imgPath == "")
            {
                imgPath = OldPath;
            }


            manif.Name = Name;

            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            Manifestation manifestation = manifestations.Find(x => x.Name == manif.Name);

            if (!ModelState.IsValid)
            {
                Debug.WriteLine("[DEBUG][{0}] Invalid manifestation registration", DateTime.Now);
                var errors = ModelState.Select(x => x.Value.Errors)
                       .Where(y => y.Count > 0).ToList();
                List<string> errorMsg = new List<string>();
                foreach (var error in errors)
                {
                    foreach (var e in error)
                    {
                        Debug.WriteLine(e.ErrorMessage);
                        errorMsg.Add(e.ErrorMessage);
                    }

                }
                return View("ManifestationsError", errorMsg);

            }


            manifestation.RegularCost = manif.RegularCost;
            manifestation.RegularSeats = manif.RegularSeats;
            manifestation.FanPitSeats = manif.FanPitSeats;
            manifestation.VipSeats = manif.VipSeats;
            manifestation.Type = manif.Type;
            manifestation.EventTime = DateTime.Parse(manif.EventTime.ToShortDateString() + " " + Time); 
            manifestation.Active = true;
            manifestation.OrganiserUsername = user.Username;
            EventPlace place = new EventPlace()
            {
                Street = Street,
                Town = Town,
                ZipCode = ZipCode
            };
            Location location = new Location()
            {
                Place = place,
                GeoHeight = double.Parse(GeoHeight),
                GeoWidht = double.Parse(GeoWidht)
            };
            manifestation.Location = location;
            manifestation.ImagePath = imgPath;

            manifestations.Remove(manifestation);

            if (manifestations.Exists(x => x.Location.Place.Equals(manifestation.Location.Place)
                && x.EventTime.ToShortDateString() == manifestation.EventTime.ToShortDateString()))
            {
                List<string> msgs = new List<string>
                {
                    "That location is arleady taken for that date."
                };
                return View("ManifestationsError", msgs);
            }
            manifestations.Add(manifestation);
            Storage.SaveManifestations(manifestations);


            return RedirectToAction("DisplayManifestations");
        }

        
        public ActionResult Comments(string Name)
        {
            User user = (User)Session["UserData"];

            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            Manifestation manif = manifestations.Find(x => x.Name == Name);

            CommentsViewModel viewModel = new CommentsViewModel()
            {
                User = user,
                Manifestation = manif
            };

            return View(viewModel);

        }

        [HttpPost]
        public ActionResult PostComment(string score, string Name)
        {
            string text = Request.Form["newComment"];
            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            Manifestation manif = manifestations.Find(x => x.Name == Name);
            User user = (User)Session["UserData"];

            if (score == null ||  score == "")
            {
                List<string> errorMsg = new List<string>()
                {
                    "Score not selected."
                };
                return View("ManifestationsError", errorMsg);
            }

            Comment comment = new Comment()
            {
                Active = true,
                Manifestation = manif,
                Score = int.Parse(score),
                Text = text,
                User = user,
            };
            manif.Comments.Add(comment);

            Storage.SaveManifestations(manifestations);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult FilterManifestations(string ManifestationName, double costLower, double costHigher,
            string dateLower, string dateHigher, string Town, string Type)
        {


            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            List<Manifestation> filteredManifs = manifestations;

            if (Type != null && Type != "")
            {
                ManifestationType type = (ManifestationType)Enum.Parse(typeof(ManifestationType), Type);
                filteredManifs = filteredManifs.Where(x => x.Type == type).ToList();
            }

            string seats = Request["Seats"];
            if (seats != null && seats == "True")
            {
                filteredManifs = filteredManifs.Where(x => x.SeatCount > 0).ToList();
            }


            if (ManifestationName != null && ManifestationName != "")
            {
                filteredManifs = filteredManifs.Where(x =>
                    x.Name.Contains(ManifestationName)).ToList();
            }

            if (Town != null && Town != "")
            {
                filteredManifs = filteredManifs.Where(x => x.Location.Place.Town.Contains(Town)).ToList();
            }

            DateTime lower;
            DateTime higher;
            try
            {
                lower = DateTime.Parse(dateLower);
                higher = DateTime.Parse(dateHigher);

                filteredManifs = filteredManifs.Where(x =>
                        x.EventTime >= lower && x.EventTime <= higher).ToList();

            }
            catch { };

            if (!(costHigher == 0 && costLower == 0))
            {
                filteredManifs = filteredManifs.Where(x => x.RegularCost <= costHigher && x.RegularCost >= costLower).ToList();
            }



            if ((ManifestationName == null || ManifestationName == "") && (costHigher == 0 && costLower == 0)
                 && (dateLower == null || dateHigher == null)  && (Town == null || Town == "")
                 && (Type == null || Type == ""))
            {
                HttpContext.Application["FilteredManifestations"] = false;
            }
            else
            {
                HttpContext.Application["FilteredManifestations"] = true;
                HttpContext.Application["FilteredManifestationsList"] = filteredManifs;
            }




            return View("Index", filteredManifs);
        }

        
        public ActionResult SortName() {
            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];

            bool filtered = (bool)HttpContext.Application["FilteredManifestations"];
            if (filtered)
            {
                manifestations = (List<Manifestation>)HttpContext.Application["FilteredManifestationsList"];
            }


            bool sorted = (bool)HttpContext.Application["SortNameManifestation"];

            if (sorted)
            {
                manifestations = (List<Manifestation>)HttpContext.Application["manifsSortedName"];
                manifestations.Reverse();
                HttpContext.Application["SortNameManifestation"] = false;
            }
            else
            {
                manifestations = manifestations.OrderBy(x => x.Name).ToList();
                HttpContext.Application["manifsSortedName"] = manifestations;
                HttpContext.Application["SortNameManifestation"] = true;
            }
            HttpContext.Application["SortLocationManifestation"] = false;
            HttpContext.Application["SortCostManifestation"] = false;
            HttpContext.Application["SortTimeManifestation"] = false;

            return View("Index", manifestations);
        }


      
        public ActionResult SortTime()
        {
            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];

            bool filtered = (bool)HttpContext.Application["FilteredManifestations"];
            if (filtered)
            {
                manifestations = (List<Manifestation>)HttpContext.Application["FilteredManifestationsList"];
            }


            bool sorted = (bool)HttpContext.Application["SortTimeManifestation"];

            if (sorted)
            {
                manifestations = (List<Manifestation>)HttpContext.Application["manifsSortedTime"];
                manifestations.Reverse();
                HttpContext.Application["SortTimeManifestation"] = false;
            }
            else
            {
                manifestations = manifestations.OrderBy(x => x.EventTime).ToList();
                HttpContext.Application["manifsSortedTime"] = manifestations;
                HttpContext.Application["SortTimeManifestation"] = true;
            }
            HttpContext.Application["SortNameManifestation"] = false;
            HttpContext.Application["SortLocationManifestation"] = false;
            HttpContext.Application["SortCostManifestation"] = false;
            return View("Index", manifestations);
        }


     
        public ActionResult SortLocation()
        {
            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];

            bool filtered = (bool)HttpContext.Application["FilteredManifestations"];
            if (filtered)
            {
                manifestations = (List<Manifestation>)HttpContext.Application["FilteredManifestationsList"];
            }


            bool sorted = (bool)HttpContext.Application["SortLocationManifestation"];

            if (sorted)
            {
                manifestations = (List<Manifestation>)HttpContext.Application["manifsSortedLocation"];
                manifestations.Reverse();
                HttpContext.Application["SortLocationManifestation"] = false;
            }
            else
            {
                manifestations = manifestations.OrderBy(x => x.Location.Place.Town).ToList();
                HttpContext.Application["manifsSortedLocation"] = manifestations;
                HttpContext.Application["SortLocationManifestation"] = true;
            }
            HttpContext.Application["SortNameManifestation"] = false;
            HttpContext.Application["SortCostManifestation"] = false;
            HttpContext.Application["SortTimeManifestation"] = false;
            return View("Index", manifestations);
        }


        
        public ActionResult SortCost()
        {
            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];

            bool filtered = (bool)HttpContext.Application["FilteredManifestations"];
            if (filtered)
            {
                manifestations = (List<Manifestation>)HttpContext.Application["FilteredManifestationsList"];
            }


            bool sorted = (bool)HttpContext.Application["SortCostManifestation"];

            if (sorted)
            {
                manifestations = (List<Manifestation>)HttpContext.Application["manifsSortedCost"];
                manifestations.Reverse();
                HttpContext.Application["SortCostManifestation"] = false;
            }
            else
            {
                manifestations = manifestations.OrderBy(x => x.RegularCost).ToList();
                HttpContext.Application["manifsSortedCost"] = manifestations;
                HttpContext.Application["SortCostManifestation"] = true;
            }
            HttpContext.Application["SortNameManifestation"] = false;
            HttpContext.Application["SortLocationManifestation"] = false;
            HttpContext.Application["SortTimeManifestation"] = false;
            return View("Index", manifestations);
        }

        [HttpPost]
        public ActionResult ChangeComment(string Name, string Username)
        {
            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            Manifestation manif = manifestations.Find(x => x.Name == Name);

            Comment comment = manif.Comments.Find(x => x.User.Username == Username);

            if (comment is null)
            {
                //TODO vristi
            }

            comment.Active = !comment.Active;
            Storage.SaveManifestations(manifestations);


            return RedirectToAction("Comments", new { Name });
        }
    }
}