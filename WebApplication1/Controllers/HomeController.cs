﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals(""))
                return RedirectToAction("Index", "Authentication");


            return View(user);
        }

        
    }
}