﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals(""))
                return RedirectToAction("Index", "Authentication");


            return View(user);
        }


        [HttpPost]
        public ActionResult Change(User user)
        {
            List<User> users = (List<User>)HttpContext.Application["users"];

            if (!ModelState.IsValid)
            {
                Debug.WriteLine("[DEBUG][{0}] Invalid registration attempt", DateTime.Now);
                var errors = ModelState.Select(x => x.Value.Errors)
                       .Where(y => y.Count > 0).ToList();
                List<string> errorMsg = new List<string>();
                foreach (var error in errors)
                {
                    foreach (var e in error)
                    {
                        Debug.WriteLine(e.ErrorMessage);
                        errorMsg.Add(e.ErrorMessage);
                    }

                }
                return View("UserError", errorMsg);

            }


            User changedUser = (User)Session["UserData"];
            changedUser.Password = user.Password;
            changedUser.Name = user.Name;
            changedUser.Lastname = user.Lastname;
            changedUser.Gender = user.Gender;
            changedUser.DateOfBirth = user.DateOfBirth;


            users[users.FindIndex(x => x.Username == changedUser.Username)] = changedUser;

            Storage.SaveUsers(users);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult DisplayTickets()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Customer)
                return RedirectToAction("Index", "Authentication");

            return View(user.Tickets);

        }

        [HttpPost]
        public ActionResult ForfeitTicket(string ticketId)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Customer)
                return RedirectToAction("Index", "Authentication");

            List<Ticket> tickets = (List<Ticket>)HttpContext.Application["tickets"];

            Ticket ticket = tickets.Find(x => x.Id == ticketId);
            if (ticket is null)
            {
                return RedirectToAction("DisplayTickets");
            }


            if ((ticket.EventTime  - DateTime.Now).TotalDays < 7)
            {
                List<string> errorMsg = new List<string>()
                {
                    "The event is too soon to be canceled!"
                };
                return View("UserError", errorMsg);
            }

            user.Points -= (int)(ticket.Cost / 1000 * 133 * 4);
            user.Points = Math.Max(user.Points, 0);
            ticket.Status = TicketStatus.Forfeit;

            switch (ticket.Type)
            {
                case TicketType.VIP:
                    ticket.Manifestation.VipSeats--;
                    break;
                case TicketType.REGULAR:
                    ticket.Manifestation.RegularSeats--;
                    break;
                case TicketType.FANPIT:
                    ticket.Manifestation.FanPitSeats--;
                    break;
            }

            List<Manifestation> manifestations = (List<Manifestation>)HttpContext.Application["manifestations"];
            Storage.SaveManifestations(manifestations);


            List<User> users = (List<User>)HttpContext.Application["users"];
            User toChange = users.Find(x => x.Username == user.Username);
            toChange.Points = user.Points;
            Storage.SaveTickets(tickets);
            Storage.SaveUsers(users);

            return RedirectToAction("DisplayTickets");
        }

        [HttpPost]
        public ActionResult FilterTickets(string ManifestationName, double costLower, double costHigher,
            string dateLower, string dateHigher, string status)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Customer)
                return RedirectToAction("Index", "Authentication");


            List<Ticket> filteredTickets = user.Tickets;
            
            

            if(ManifestationName != null && ManifestationName != "")
            {
                filteredTickets = filteredTickets.Where(x =>
                    x.Manifestation.Name.Contains(ManifestationName)).ToList();
            }

            DateTime lower;
            DateTime higher;
            try
            {
                lower = DateTime.Parse(dateLower);
                higher = DateTime.Parse(dateHigher);

                filteredTickets = filteredTickets.Where(x => 
                        x.Manifestation.EventTime >= lower &&  x.Manifestation.EventTime <= higher).ToList();

            }
            catch { };

            if (!(costHigher == 0 && costLower == 0))
            {
                filteredTickets = filteredTickets.Where(x => x.Cost <= costHigher && x.Cost >= costLower).ToList();
            }

            if (status == "Reserved")
            {
                filteredTickets = filteredTickets.Where(x => x.Status == TicketStatus.Reserved).ToList();
            }
            else if (status == "Forfeit") {

                filteredTickets = filteredTickets.Where(x => x.Status == TicketStatus.Forfeit).ToList();
            }


            return View("DisplayTickets", filteredTickets);
        }
    }
}